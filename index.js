"use stricts";
const cities = [
  "Kiev",
  "Kharkiv",
  ["4mobik", "Glory to Ukraine!", 98, false],
  "Odessa",
  "Lviv",
  ["lol", "kek", 22, 888],
  "Dnipro",
  "Donetsk",
];
let parent = document.createElement("ul");

function getList(array, parent) {
  if (parent !== undefined) {
    for (let element of array) {
      const listElement = document.createElement("li");
      listElement.innerHTML = element;
      parent.append(listElement);
      document.body.append(parent);

      const ul = document.createElement("ul");
      if (Array.isArray(element)) {
        listElement.remove();
        element.map((el) => {
          const li = document.createElement("li");
          li.innerHTML = el;
          ul.append(li);
          parent.append(ul);
        });
      }
    }
  } else if (parent === undefined) {
    parent = document.body;
    for (let element of array) {
      const listElement = document.createElement("li");
      listElement.innerHTML = element;
      document.body.append(listElement);

      const ul = document.createElement("ul");
      if (Array.isArray(element)) {
        listElement.remove();
        element.map((el) => {
          const li = document.createElement("li");
          li.innerHTML = el;
          ul.append(li);
          parent.append(ul);
        });
      }
    }
  }
}

getList(cities, parent);

const clearPage = () => (document.body.innerHTML = "");

setTimeout(clearPage, 3000);
